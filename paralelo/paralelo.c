#include <stdlib.h>
#include <stdio.h>
#include <netcdf.h>
#include <omp.h>

/* Handle errors by printing an error message and exiting with a non-zero status. */
#define ERRCODE 2
#define ERR(e) {printf("Error: %s\n", nc_strerror(e)); exit(ERRCODE);}

/* Name of the files to read/write */
#define IN_FILE_NAME "DATA_INPUT.nc"
#define OUT_FILE_NAME "DATA_OUTPUT.nc"

/* Matrix X-Y dimentions, Kernel X-Y dimentions and Number of dimentions */
#define NX 21696
#define NY 21696
#define KX 3
#define KY 3
#define NDIMS 2

float data_in[NX][NY], data_out[NX][NY];

int main(int argc, char const *argv[])
{
	
	if ( argc <2 ) {
        printf("Operacion Invalida: Utilice ./paralelo <cantidad de hilos> \n");
		exit( 1 );
	}

	int num_threads=atoi(argv[1]);
	
	size_t chunksize[NDIMS] = {2048,2048};
    int old_fill_mode, ncid, varid, retval, mm, nn, j, i, m, n, ii, jj, x_dimid, y_dimid, dimids[NDIMS], kCenterX, kCenterY, kernel[3][3] = {{-1, -1, -1},
																																			 {-1, 8, -1},
																																			 {-1, -1, -1}};
	/* Opening NetCDF File */
    if ((retval = nc_open(IN_FILE_NAME, NC_NOWRITE, &ncid)))
        ERR(retval);
    
    /* Obtaining varID of variable CMI. */
    if ((retval = nc_inq_varid(ncid, "CMI", &varid)))
        ERR(retval);

    /* Reading all Matrix in one operation */
    if ((retval = nc_get_var_float(ncid, varid, &data_in[0][0])))
        ERR(retval);
    
    /* Doing the convolution between huge 2D array and kernel */	
	kCenterX = KX/2;
	kCenterY = KY/2;

omp_set_num_threads(num_threads);
#pragma omp parallel for private(mm,nn,ii,jj,m,n) collapse(2) schedule(static)
for (i = 0; i < NY; ++i) /* rows */
{
  for (j = 0; j < NX; ++j) /* columns */
  {
    for (m = 0; m < KY; ++m) /* kernel rows */
    {
      if (data_in[i][j] <= 0) {
        data_out[i][j] = data_in[i][j];
      } else {
        mm = KY - 1 - m; /* row index of flipped kernel */

        for (n = 0; n < KY; ++n) /* kernel columns */
        {
          nn = KX - 1 - n; /* column index of flipped kernel */

          /* index of input signal, used for checking boundary */
          ii = i + (kCenterY - mm);
          jj = j + (kCenterX - nn);

          /* ignore input samples which are out of bound */
          if (ii >= 0 && ii < NY && jj >= 0 && jj < NX)
            data_out[i][j] += data_in[ii][jj] * kernel[mm][nn];
        }
      }
    }
  }
}

    /* Closing NetCDF File */
    if ((retval = nc_close(ncid)))
        ERR(retval);  

    /* Create the file. The NC_CLOBBER parameter tells netCDF to overwrite this file, if it already exists.*/
   if ((retval = nc_create(OUT_FILE_NAME, NC_NETCDF4|NC_CLOBBER, &ncid)))
      ERR(retval);
  
    /* Write data with no prefilling -- Important discovery */
   if ((retval = nc_set_fill(ncid, NC_NOFILL, &old_fill_mode)))
      ERR(retval);
  
    /* Define the dimensions. NetCDF will hand back an ID for each. */
   if ((retval = nc_def_dim(ncid, "x", NX, &x_dimid)))
      ERR(retval);
   if ((retval = nc_def_dim(ncid, "y", NY, &y_dimid)))
      ERR(retval);
  
	/* The dimids array is used to pass the IDs of the dimensions of the variable. */
   dimids[0] = y_dimid;
   dimids[1] = x_dimid;

	/* Define the variable. The type of the variable in this case is NC_FLOAT */
   if ((retval = nc_def_var(ncid, "CMI", NC_FLOAT, NDIMS, dimids, &varid)))
      ERR(retval);
  
   if ((retval = nc_def_var_chunking(ncid, varid, NC_CHUNKED, chunksize)))
	   ERR(retval);
   
	/* End define mode. This tells netCDF we are done defining metadata */
   if ((retval = nc_enddef(ncid)))
      ERR(retval);
  
   /* Write the pretend data to the file. Although netCDF supports
    * reading and writing subsets of data, in this case we write all
    * the data in one operation. */
   if ((retval = nc_put_var_float(ncid, varid, &data_out[0][0])))
      ERR(retval);
  
   /* Close the file. This frees up any internal netCDF resources
    * associated with the file, and flushes any buffers. */
   if ((retval = nc_close(ncid)))
      ERR(retval);

    return 0;
}