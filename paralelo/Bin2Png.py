#!/usr/bin/env python

import matplotlib.pyplot as plt
import matplotlib.colors
import netCDF4

# open a local NetCDF file or remote OPeNDAP URL
fp = 'DATA_OUTPUT.nc'
nc = netCDF4.Dataset(fp)

# examine the variables
print nc.variables.keys()
print nc.variables['CMI']

# sample every single point of the 'CMI' variable
topo = nc.variables['CMI'][::,::]

# make image
plt.figure(dpi=2500)
plt.imshow(topo, cmap='gray_r',origin='lower', vmin=0, vmax=5000)

# print color bar
plt.colorbar()

# save image as png
plt.savefig('image.png', bbox_inches=0)