#!/bin/bash

filename="EXECUTION_RESULTS.txt"

> $filename

echo "A continuacion se realizan 30 ejecuciones secuenciales para obtener metricas.."
for i in {1..30}
do 
   echo $i": Iniciado la medicion con 2 hilos"
	 echo "Ejecucion con 2 hilos" >> $filename
    (time ./paralelo 2) 2>> $filename
   echo $i": Medicion concluida"
   
   echo $i": Iniciado la medicion con 4 hilos"
	 echo "Ejecucion con 4 hilos" >> $filename
    (time ./paralelo 4) 2>> $filename
   echo $i": Medicion concluida"
   
   echo $i": Iniciado la medicion con 8 hilos"
	 echo "Ejecucion con 8 hilos" >> $filename
    (time ./paralelo 8) 2>> $filename
   echo $i": Medicion concluida"
   
   echo $i": Iniciado la medicion con 16 hilos"
	 echo "Ejecucion con 16 hilos" >> $filename
    (time ./paralelo 16) 2>> $filename
   echo $i": Medicion concluida"
   
   echo $i": Iniciado la medicion con 32 hilos"
	 echo "Ejecucion con 32 hilos" >> $filename
    (time ./paralelo 32) 2>> $filename
   echo $i": Medicion concluida"
   
   echo $i": Iniciado la medicion con 64 hilos"
	 echo "Ejecucion con 64 hilos" >> $filename
    (time ./paralelo 64) 2>> $filename
   echo $i": Medicion concluida"
   
   echo $i": Iniciado la medicion con 128"
	 echo "Ejecucion con 128 hilos" >> $filename
    (time ./paralelo 128) 2>> $filename
   echo $i": Medicion concluida"         
   
done

echo "Resultados disponibles en " $filename


