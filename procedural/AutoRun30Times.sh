#!/bin/bash

filename="EXECUTION_RESULTS.txt"

> $filename

echo "A continuacion se realizan 30 ejecuciones secuenciales para obtener metricas.."
for i in {1..30}
do 
   echo $i": Iniciado la medicion"
    (time ./procedural) 2>> $filename
   echo $i": Medicion concluida"

	sleep 5
done

echo "Resultados disponibles en " $filename


